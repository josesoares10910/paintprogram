import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class InputHandler implements KeyboardHandler {

    private final Keyboard keyboard;
    private final KeyboardEvent[] events;
    private Rectangle miniRectangle;
    private boolean pressed;
    private Cursor cursor;
    private Map map;
    private SaveGame saveGame;



    public InputHandler(Cursor cursor) {
        map = cursor.map;
        keyboard = new Keyboard(this);
        events = new KeyboardEvent[10];
        miniRectangle = Cursor.miniRectangle;
        this.cursor = cursor;
        createEvents();
        saveGame = new SaveGame(map);
    }

    public void createEvents(){

        for (int i = 0; i < events.length; i++) {
            events[i] = new KeyboardEvent();
        }

        events[0].setKey(KeyboardEvent.KEY_DOWN);
        events[1].setKey(KeyboardEvent.KEY_UP);
        events[2].setKey(KeyboardEvent.KEY_RIGHT);
        events[3].setKey(KeyboardEvent.KEY_LEFT);
        events[4].setKey(KeyboardEvent.KEY_SPACE);
        events[5].setKey(KeyboardEvent.KEY_C);
        events[6].setKey(KeyboardEvent.KEY_K);
        events[7].setKey(KeyboardEvent.KEY_L);
        events[8].setKey(KeyboardEvent.KEY_Q);

        for(KeyboardEvent event : events){
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(event);
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                if ((miniRectangle.getY() - Grid.tileSize) > 0) {
                    miniRectangle.translate(0, -Grid.tileSize);
                }
                break;

            case KeyboardEvent.KEY_DOWN:
                if ((miniRectangle.getY() + Grid.tileSize) < Grid.fieldHeight) {
                    miniRectangle.translate(0, Grid.tileSize);
                }
                break;

            case KeyboardEvent.KEY_RIGHT:
                if ((miniRectangle.getX() + Grid.tileSize) < Grid.fieldWidth) {
                    miniRectangle.translate(Grid.tileSize, 0);
                }
                break;

            case KeyboardEvent.KEY_LEFT:
                if ((miniRectangle.getX() - Grid.tileSize) > 0) {
                    miniRectangle.translate(-Grid.tileSize, 0);
                }
                break;

            case KeyboardEvent.KEY_SPACE:
                map.paintTile();
                break;

            case KeyboardEvent.KEY_C:
                map.deleteTile();
                break;

            case KeyboardEvent.KEY_K:
                saveGame.save();
                break;

            case KeyboardEvent.KEY_L:
                saveGame.load();
                break;

            case KeyboardEvent.KEY_Q:
                System.exit(0);
                break;

            case KeyboardEvent.KEY_1:
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public boolean isPressed() {
        return pressed;
    }

    public void setPressed(boolean pressed) {
        this.pressed = pressed;
    }
}
