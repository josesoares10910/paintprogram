import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Map {

    private final Rectangle rectangle;
    public Tile[][] gridRectangles;
    public static  Cursor cursor;


    public Map() {
        rectangle = new Rectangle(Grid.padding, Grid.padding, Grid.fieldWidth - Grid.padding, Grid.fieldHeight - Grid.padding);
        rectangle.draw();

        loadGrid();
        cursor = new Cursor(this);
    }

    public void loadGrid() {

        gridRectangles = new Tile[Grid.fieldMaxRow][Grid.fieldMaxCol];

        for (int i = 0; i < gridRectangles.length; i++) {
            for (int j = 0; j < gridRectangles[i].length; j++) {
                gridRectangles[i][j] = new Tile(Grid.padding + j * Grid.tileSize, Grid.padding + i * Grid.tileSize, Grid.tileSize, Grid.tileSize);
                gridRectangles[i][j].draw();
            }
        }

    }

    public void paintTile(){

        int col = (Cursor.miniRectangle.getX() - Grid.padding)/Grid.tileSize;
        int row = (Cursor.miniRectangle.getY() - Grid.padding)/Grid.tileSize;

        gridRectangles[row][col].fill();
    }

    public void deleteTile(){

        int col = (Cursor.miniRectangle.getX() - Grid.padding)/Grid.tileSize;
        int row = (Cursor.miniRectangle.getY() - Grid.padding)/Grid.tileSize;

        gridRectangles[row][col].draw();
    }
}

