
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cursor{

    public static Rectangle miniRectangle;

    public Map map;


    public Cursor(Map map){
        this.map = map;
        miniRectangle = new Rectangle(Grid.padding,Grid.padding,Grid.tileSize,Grid.tileSize);
        miniRectangle.draw();
        miniRectangle.setColor(Color.CYAN);
        miniRectangle.fill();
    }

}
