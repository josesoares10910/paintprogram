
public class Grid {

    public static final int tileSize = 20;
    public static final int fieldMaxRow = 30;
    public static final int fieldMaxCol = 30;
    public static final int padding = 10;
    public static final int fieldHeight = tileSize * fieldMaxRow + padding;
    public static final int fieldWidth = tileSize * fieldMaxCol + padding;
}
