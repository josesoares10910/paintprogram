import java.io.*;

public class SaveGame {

    private Map map;

    public SaveGame(Map map){
        this.map = map;
    }


    public void save() {
        try {
            FileWriter fileWriter = new FileWriter("resources/grid.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(int i = 0; i < map.gridRectangles.length; i++) {
                for (int j = 0; j < map.gridRectangles[i].length; j++) {

                    if (map.gridRectangles[i][j].isFilled()){
                        bufferedWriter.write("1");
                    } else {
                        bufferedWriter.write("0");
                    }
                }
                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(){

        try {
            FileReader fileReader = new FileReader("resources/grid.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String result = "";
            String[] stringArray = new String[Grid.fieldMaxRow];
            int[][] intArray = new int[Grid.fieldMaxRow][Grid.fieldMaxCol];

            int row = 0;

            while ((line = bufferedReader.readLine()) != null) {

                stringArray = line.split("");

                for(int col = 0; col < stringArray.length; col++) {

                    int num = Integer.parseInt(stringArray[col]);
                    System.out.println(num);
                    System.out.println("String " +  row);
                    if(num == 0){
                        map.gridRectangles[row][col].draw();
                    } else if (num == 1) {
                        map.gridRectangles[row][col].fill();
                    }
                }
                row ++;
            }

            bufferedReader.close();

            } catch(IOException e){
                throw new RuntimeException(e);
            }
    }
}


