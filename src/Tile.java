import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Tile extends Rectangle {

    private boolean paint;

    public Tile(int x, int y, int width, int height){
        super(x, y, width, height);
        paint = false;
    }

    public boolean isPaint() {
        return paint;
    }

    public void setPaint(boolean paint) {
        this.paint = paint;
    }
}
